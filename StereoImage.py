#!/usr/bin/python
# coding=UTF-8
# -*- UTF-8 -*-



import os, sys
from PIL import Image
import subprocess
import tempfile
import shutil
from StringIO import StringIO

class StereoImage:
	FFMPEG_CMD = 'ffmpeg'
	left = None
	right = None

	def __init__(self):
		pass

	def __save__(self, image, file, type='png', JpgQuality=100):
		if type == 'jpg':
			image.save(file, 'JPEG', quality=JpgQuality)
		if type == 'png':
			image.save(file, 'PNG')

	def fromMpo(self, mpofile):
		# Lot of the code copy stolen and copy-pasted from:
		# http://www.chrisevans3d.com/pub_blog/splitting-mpo-files-with-exiftool-and-python/ + comments

		# get left and right images from .MPO file (based on C code from Andres Hernandez):-
		lfile = open(mpofile, 'rb')
		self.left = Image.open(lfile)
		self.left.load()
		lfile.seek(4)  # skip SOI and APP1 markers of 1st image
		data = lfile.read()  # read both images

		offset = data.find('\xFF\xD8\xFF\xE1')  # find SOI and APP1 markers of 2nd image
		rfile = StringIO(data[offset:])
		self.right = Image.open(rfile)
		self.right.load()
		lfile.close()
		rfile.close()

	'''
	def fromSBS(self, sbsfile):
		# get left and right images from .MPO file (based on C code from Andres Hernandez):-
		lfile = open(mpofile, 'rb')
		self.left = Image.open(lfile)
		self.left.load()
		lfile.seek(4)  # skip SOI and APP1 markers of 1st image
		data = lfile.read()  # read both images

		offset = data.find('\xFF\xD8\xFF\xE1')  # find SOI and APP1 markers of 2nd image
		rfile = StringIO(data[offset:])
		self.right = Image.open(rfile)
		self.right.load()
		lfile.close()
		rfile.close()
	'''

	# probably most common file format for stereo images
	def saveSBS(self, output, type='png', JpgQuality=100):
		size = self.left.size
		out = self.left.resize(((size[0] * 2), size[1]), Image.NEAREST)
		out.paste(self.left, (0, 0))
		out.paste(self.right, (size[0], 0))
		self.__save__(out, output, type, JpgQuality)

	# for  odd colored gogles
	def saveAnaglyph(self, output, type='png', format='RCH', JpgQuality=100):
		_left = self.left.split()
		_right = self.right.split()
		out = None
		# red-cyan
		if format == 'RC':
			out = Image.merge('RGB', (_left[0], _right[1], _right[2]))
		# red-cyan half-color
		if format == 'RCH':
			lgray = self.left.convert('L')
			rgray = self.right.convert('L')
			hcr = Image.blend(lgray, _left[0], 0.50)
			hcg = Image.blend(rgray, _right[1], 0.50)
			hcb = Image.blend(rgray, _right[1], 0.50)
			out = Image.merge('RGB', (hcr, hcg, hcb))
		# blue-magenta
		if format == 'BM':
			out = Image.merge('RGB', (_right[0], _right[1], _left[2]))
		# blue-magenta half-color
		if format == 'BMH':
			lgray = self.left.convert('L')
			rgray = self.right.convert('L')
			hcr = Image.blend(rgray, _right[0], 0.50)
			hcg = Image.blend(rgray, _right[1], 0.50)
			hcb = Image.blend(lgray, _left[1], 0.50)
			out = Image.merge('RGB', (hcr, hcg, hcb))
		self.__save__(out, output, type, JpgQuality)

	# WebM is used for streaming 3D video -> internet compatiblestereo viewing/streaming
	# (afaik nvidia only supports viewing videos through web, so encode image as static video)
	def saveWebM(self, output, format='SBS'):
		#save image as tmp files
		tmpdir = tempfile.mkdtemp()
		self.saveSBS(tmpdir + "/tmpsbs-1.png", type='png')
		self.saveSBS(tmpdir + "/tmpsbs-2.png", type='png')
		# encode
		# ffmpeg -framerate 1/10 -i input.png -r 25 -c:v libvpx -b:v 5M -an video.webm
		cmd = [self.FFMPEG_CMD, '-framerate', '1/10', '-i', tmpdir + '/tmpsbs-%01d.png', '-r', '25', '-c:v', 'libvpx', '-b:v', '5M', '-metadata:s:v:0', 'stereo_mode=left_right', '-an', output]
		subprocess.call(cmd)
		#remove tmpfiles
		shutil.rmtree(tmpdir)
