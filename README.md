# README #

Tool for converting stereo images from mpo to other formats.

## Requirements ##

* Python (tested with 2.7)
* PIL
* ffmpeg with libvpx enabled

## scripts

Convert .mpo files to half-color red-cyan anaglyph, side-by-side image and static side-by-side webm-videos.

color images are more presentable (imho) as half-color when using anaglyph. Side-by-side formats are better for native stereo viewing equipment. WebM used to be supported by nvidia browser-plugin for showing 3D-stereo video content on web-pages.

### StereoImage.py

Python class that handles file format and color conversions.
 
### mpo2sbs.py

Takes a list of files as an input. This works in windows by dragging and dropping .mpo files to script.

Outputs are written to the same directory as input files with _RCH.png, _SBS.pns and _SBS.webm extensions for anaglyph and side-by-side images and webm videos.

### mpo2others2.py

Takes a path as an input and converts all .mpo files found in the given directory. Works better in linux commandline.

Outputs are written to the same directory as input files with _RCH.png, _SBS.pns and _SBS.webm extensions for anaglyph and side-by-side images and webm videos.