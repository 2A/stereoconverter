#!/usr/bin/python
# coding=UTF-8
# -*- UTF-8 -*-

import os, sys

from StereoImage import StereoImage

if __name__ == "__main__":
	inpath = sys.argv[1]
	print inpath
	abspath = os.path.abspath(inpath)
	#bn = os.path.splitext()[0]
	files = os.listdir(abspath)
	print 'Processing', len(files), 'file(s).'
	for f in files:
		if (os.path.splitext(f)[1] == '.MPO' or os.path.splitext(f)[1] == '.mpo'):
			print "Processing:", f,
			#bn = os.path.splitext( os.path.basename(f))[0]
			# pil wont save files with relative path
			bn = abspath + '/' + os.path.splitext(f)[0]
			sim = StereoImage()
			sim.fromMpo(abspath + '/' + f)
			print bn
			sim.saveAnaglyph(bn + '_RCH.png', type='png', format='RCH')
			sim.saveWebM(bn + '_SBS.webm')
			sim.saveSBS(bn + '_SBS.pns', type='png')
		else:
			print f, "is not an .mpo file. Skipped."
	#input('All done. Press Return to exit.')
	print 'All done.'
	